@extends('layouts.app')

@section('content')

    
<div class="container">
    <div class="card">
        <div class="card-header bg-dark text-white">Edit Category</div>
        <div class="card-body">
            <form action="{{route('admin.categories.update', $category->id)}}" method="post">
                @csrf
                @method('PUT')
                <x-input label="Category Name" type="text" field="name" current="{{$category->name}}" />
                <input type="submit" class="btn btn-primary" value="Update">
            </form>
        </div>
    </div>
</div>


@endsection