@extends('layouts.app')

@section('content')

    <div class="container">
        <x-alert />
        <div class="card">
            <div class="card-header bg-dark text-white d-flex justify-content-between">
                <span>All Categories</span>
                <span class="d-flex">
                    <form action="{{route('admin.categories.search')}}">
                        <div class="input-group">
                            <input type="text" name="q" value="{{request('q')}}" class="form-control form-control-sm">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-sm btn-outline-success">Search</button>
                            </div>
                        </div>
                    </form>
                    <a href="{{route('admin.categories.create')}}" class="btn btn-sm btn-primary ml-2">Add Category</a>
                </span> 
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Actions</th>
                    </tr>
                    @foreach ($categories as $category)
                        <tr>
                            <td>{{$category->id}}</td>
                            <td>{{$category->name}}</td>
                            <td>
                                <a href="{{route('admin.categories.show', $category->id)}}" class="btn btn-sm btn-info">View</a>
                                <a href="{{route('admin.categories.edit', $category->id)}}" class="btn btn-sm btn-success">Edit</a>
                                <form style="display: inline" action="{{route('admin.categories.destroy', $category->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection