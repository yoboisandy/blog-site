@extends('layouts.app')

@section('content')

    
<div class="container">
    <x-alert />
    <div class="card">
        <div class="card-header bg-dark text-white">Add Category
            <a class="btn btn-sm btn-primary float-right" href="{{route('admin.categories.index')}}">Go Back</a>
        </div>
        <div class="card-body">
            <form action="{{route('admin.categories.store')}}" method="post">
                @csrf
                <x-input field="name" type="text" label="Category Name" />
                <input type="submit" class="btn btn-primary" value="Add">
            </form>
        </div>
    </div>
</div>


@endsection