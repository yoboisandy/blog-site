@extends('layouts.app')

@section('content')

    
<div class="container">
    <x-alert />
    <div class="card">
        <div class="card-header bg-dark text-white">Add Post
            <a class="btn btn-sm btn-primary float-right" href="{{route('admin.posts.index')}}">Go Back</a>
        </div>
        <div class="card-body">
            <form action="{{route('admin.posts.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                <x-input field="title"  label="Post Title" />
                <x-input type="textarea" field="body"  label="Post Body" />
                {{-- <x-input field="user_id"  label="User" /> --}}

                <x-input type="file" field="image" label="image" />
                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select 
                        name="category_id" 
                        id="category_id" 
                        class="form-control 
                        @error('category_id')
                            is-invalid
                        @enderror"
                    >
                        <option disabled selected>Select Category</option>

                        @foreach ($categories as $category)                        
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach

                    </select>
                    @error('category_id')
                        <small class="form-text text-danger">{{"*".$message}}</small>
                    @enderror
                </div>
                <div class="form-group">
                    <input type="checkbox" name="is_published" id="is_published" value="1">
                    <label for="is_published">Publish this post</label>
                </div>
                <input type="submit" class="btn btn-primary" value="Add">
            </form>
        </div>
    </div>
</div>
<script src="{{asset('js/ckeditor/ckeditor.js')}}"></script>
<script>ClassicEditor
    .create( document.querySelector( '#body' ), {
        
        toolbar: {
            items: [
                'heading',
                '|',
                'bold',
                'underline',
                'italic',
                'link',
                'bulletedList',
                'numberedList',
                '|',
                'insertTable',
                'undo',
                'redo'
            ]
        },
        language: 'en',
        licenseKey: '',
        
        
        
    } )
    .then( editor => {
        window.editor = editor;

        
        
        
    } )
    .catch( error => {
        console.error( 'Oops, something went wrong!' );
        console.error( 'Please, report the following error on https://github.com/ckeditor/ckeditor5/issues with the build id and the error stack trace:' );
        console.warn( 'Build id: z5yuzh1oluyb-v333vug4xbpc' );
        console.error( error );
    } );
</script>

@endsection