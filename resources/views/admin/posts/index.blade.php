@extends('layouts.app')

@section('content')

    <div class="container">
        <x-alert />
        <div class="card">
            <div class="card-header bg-dark text-white d-flex justify-content-between">
                <span>All Posts</span>
                <span class="d-flex">
                    <form action="{{route('admin.posts.search')}}">
                        <div class="input-group">
                            <input type="text" name="q" value="{{request('q')}}" class="form-control form-control-sm">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-sm btn-outline-success">Search</button>
                            </div>
                        </div>
                    </form>
                    <a href="{{route('admin.posts.create')}}" class="btn btn-sm btn-primary ml-2">Add Post</a>
                </span> 
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Title</th>
                        <th>User</th>
                        <th>Category</th>
                        <th>Published</th>
                        <th>Action</th>
                        
                    </tr>
                    @foreach ($posts as $post)
                        <tr>
                            <td>{{$post->id}}</td>
                            <td>{{$post->title}}</td>
                            <td>{{$post->user->name}}</td>
                            <td>{{$post->category->name}}</td>
                            <td>{{$post->is_published ? "Yes" : 'No'}}</td>
                            <td>
                                <a href="{{route('admin.posts.show', $post->id)}}" class="btn btn-sm btn-info">View</a>
                                <a href="{{route('admin.posts.edit', $post->id)}}" class="btn btn-sm btn-success">Edit</a>
                                <form style="display: inline" action="{{route('admin.posts.destroy', $post)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection