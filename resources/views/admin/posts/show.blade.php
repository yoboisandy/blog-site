@extends('layouts.app')

@section('content')

    
<div class="container">
    <x-alert />
    <div class="card">
        <div class="card-header bg-dark text-white d-flex justify-content-between">
            <span>Post Details</span>
            <a class="btn btn-sm btn-primary " href="{{route('admin.posts.index')}}">Go Back</a>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Id</th>
                    <td>{{$post->id}}</td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td>{{$post->title}}</td>
                </tr>
                <tr>
                    <th>Body</th>
                    <td>{!! $post->body !!}</td>
                </tr>
                <tr>
                    <th>Image</th>
                    <th>
                        <img src="/storage/{{$post->image}}" alt="" height="100px">
                    </th>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>{{$post->category->name}}</td>
                </tr>
                <tr>
                    <th>Posted By</th>
                    <td>{{$post->user->name}}</td>
                </tr>
                <tr>
                    <th>Published</th>
                    <td>{{$post->is_published ? "Yes" : "No"}}</td>
                </tr>
                <tr>
                    <th>Posted Date</th>
                    <td>{{$post->created_at}}</td>
                </tr>
                <tr>
                    <th>Updated Date</th>
                    <td>{{$post->updated_at}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>


@endsection