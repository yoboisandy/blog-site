@extends('layouts.app')

@section('content')

    
<div class="container">
    <x-alert />
    <div class="card">
        <div class="card-header bg-dark text-white">Edit user</div>
        <div class="card-body">
            <form action="{{route('admin.users.update', $user->id)}}" method="post">
                @csrf
                @method('PUT')
                <x-input label="Name" type="text" field="name" current="{{$user->name}}"/>
                <x-input label="E-mail" type="email" field="email" current="{{$user->email}}"/>
                <x-input label="Password" type="password" field="password" />
                <div class="form-group">
                    <label for="role">Role</label>

                    <select name="role" id="role" class="form-control @error('role') is-invalid @enderror" >
                        <option disabled>Select Role</option>
                        <option value="Admin" @if ($user->role == "Admin") selected @endif>Admin</option>
                        <option value="User"  @if ($user->role == "User") selected @endif>User</option>
                    </select>
                    @error('role')
                        <small class="form-text text-danger">{{"*".$message}}</small>
                    @enderror
                </div>
                <input type="submit" class="btn btn-primary" value="Update">
            </form>
        </div>
    </div>
</div>


@endsection