@extends('layouts.app')

@section('content')  
<div class="container">
    <div class="card">
        <div class="card-header bg-dark text-white">Add user</div>
        <div class="card-body">
            <form action="{{route('admin.users.store')}}" method="post">
                @csrf
                <x-input label="Name" type="text" field="name"/>
                <x-input label="E-mail" type="email" field="email"/>
                <x-input label="Password" type="password" field="password"/>
                <label for="role">Role</label>
                <div class="form-group"> 
                    <select name="role" id="role" class="form-control @error('role') is-invalid @enderror">
                        <option disabled selected>Select Role</option>
                        <option value="Admin">Admin</option>
                        <option value="User">User</option>
                    </select>
                    @error('role')
                        <small class="form-text text-danger">{{"*".$message}}</small>
                    @enderror
                </div>
                <input type="submit" class="btn btn-primary" value="Add">
            </form>
        </div>
    </div>
</div>
@endsection