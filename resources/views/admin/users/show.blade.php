@extends('layouts.app')

@section('content')

    
<div class="container">
    <x-alert />
    <div class="card">
        <div class="card-header bg-dark text-white d-flex justify-content-between">
            <span>User Details</span>
            <a class="btn btn-sm btn-primary " href="{{route('admin.users.index')}}">Go Back</a>
        </div>
        <div class="card-body">
            <table class="table">
                <tr>
                    <th>Id</th>
                    <td>{{$user->id}}</td>
                </tr>
                <tr>
                    <th>Title</th>
                    <td>{{$user->name}}</td>
                </tr>
                <tr>
                    <th>Category</th>
                    <td>{{$user->email}}</td>
                </tr>
                <tr>
                    <th>usered By</th>
                    <td>{{$user->password}}</td>
                </tr>
                <tr>
                    <th>Published</th>
                    <td>{{$user->role}}</td>
                </tr>
                <tr>
                    <th>usered Date</th>
                    <td>{{$user->created_at}}</td>
                </tr>
                <tr>
                    <th>Updated Date</th>
                    <td>{{$user->updated_at}}</td>
                </tr>
            </table>
        </div>
    </div>
</div>


@endsection