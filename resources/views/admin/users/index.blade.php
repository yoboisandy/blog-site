@extends('layouts.app')

@section('content')

    <div class="container">
        <x-alert />
        <div class="card">
            <div class="card-header bg-dark text-white d-flex justify-content-between">
                <span>All users</span>
                <span class="d-flex">
                    <form action="{{route('admin.users.search')}}">
                        <div class="input-group">
                            <input type="text" name="q" value="{{request('q')}}" class="form-control form-control-sm">
                            <div class="input-group-append">
                                <button type="submit" class="btn btn-sm btn-outline-success">Search</button>
                            </div>
                        </div>
                    </form>
                    <a href="{{route('admin.users.create')}}" class="btn btn-sm btn-primary ml-2">Add user</a>
                </span> 
            </div>
            <div class="card-body">
                <table class="table">
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Role</th>
                        <th>Actions</th>
                    </tr>
                    @foreach ($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->role}}</td>
                            <td>
                                <a href="{{route('admin.users.show', $user->id)}}" class="btn btn-sm btn-info">View</a>
                                <a href="{{route('admin.users.edit', $user->id)}}" class="btn btn-sm btn-success">Edit</a>
                                <form style="display: inline" action="{{route('admin.users.destroy', $user->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <input type="submit" class="btn btn-sm btn-danger" value="Delete">
                                </form>
                            </td>
                        </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>

@endsection