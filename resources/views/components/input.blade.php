<div>
    @if ($type=="textarea")
    <div class="form-group">
        <label for="{{$field}}">{{$label}}</label>
        <textarea name="{{$field}}" id="{{$field}}" cols="30" class="form-control" rows="10">
            {!! old("$field") ?? $current !!}
        </textarea>
    </div>
    @else
    <div class="form-group">
        <label for="{{$field}}">{{$label}}</label>
        <input 
            type="{{$type}}" 
            name="{{$field}}" 
            id="{{$field}}" 
            class="form-control
            @error ("$field")
                is-invalid
            @enderror
            "
            value="{{old("$field") ?? $current}}"
            >
        @error("$field")
            <small class="form-text text-danger">{{"*".$message}}</small>
        @enderror
    </div>
    @endif


    
</div>