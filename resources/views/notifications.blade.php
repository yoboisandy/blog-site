@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <table>
                        @foreach (Auth::user()->notifications as $notification)
                        <tr>
                            <td>{{$notification->type}}</td>
                        </tr>
                        @endforeach                       
                </table>
            </div>
        </div>
    </div>
</div>
@endsection
