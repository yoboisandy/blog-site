@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-8">
            @foreach ($posts as $post)
            {{-- Posts Start--}}
            <div class="card mb-4">
                <div class="row no-gutters">
                    <div class="col-4">
                        @if ($post->image)
                            <img src="{{"/storage/$post->image"}}" alt="" style="width:100%;height:100%;max-height:250px;object-fit:cover;">
                        @endif
                    </div>
                    <div class="col-8">
                        <div class="card-body">
                            <h4>
                                <a href="{{route('post', $post)}}">{{$post->title}}</a>
                            </h4>
                            <p>
                                <small class="font-weight-bold">Posted By: {{$post->user->name}}</small><small class="text-muted">- On: {{$post->created_at}}</small> 
                            </p>
                            <p>
                                {!! Str::limit(strip_tags($post->body), 300, "[...]") !!}
                            </p>
                            <a href="{{route('post', $post)}}" class="btn btn-dark">
                                Continue Reading
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            {{-- Posts Start--}}
            @endforeach
            <div class="div">
                {{$posts->links()}}
            </div>
        </div>
        <div class="col-4">
            {{-- search --}}
            <div class="card mb-4">
                <div class="card-header bg-dark text-white">Search</div>
                <div class="card-body">
                    <form action="{{route('search')}}">
                        <input type="text" name="q" placeholder="Search Here" value="{{request('q')}}" class="form-control">
                    </form>
                </div>
            </div>

            {{-- Categories --}}
            <div class="card">
                <div class="card-header bg-dark text-white">Categories  <a href="{{route('index')}}" class="btn btn-sm btn-primary float-right">All Posts</a></div>
                <div class="card-body">
                    <ul>
                        @foreach ($categories as $category)
                        <li>
                            <a href="{{route('category', $category)}}">{{$category->name}}</a> 
                            ({{$category->posts_count}})
                        </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
