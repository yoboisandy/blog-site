@extends('layouts.app')

@section('content')
<div class="container">
    <div class="card">
        <div class="card-body">

            <h2>{{$post->title}}</h2>

            <div class="text-center m-3">
                <img class="img-fluid" src="/storage/{{$post->image}}" alt="" style="max-height:300px">
            </div>

            {!! $post->body !!}

            <p class="d-flex justify-content-around text-muted">
                <span>Category: {{$post->category->name}}</span>
                <span>Posted By: {{$post->user->name}}</span>
                <span>Posted on: {{$post->created_at}}</span>
            </p>
        </div>
    </div>
</div>
@endsection
