<?php

namespace Tests\Unit;

use Illuminate\Http\UploadedFile;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Storage;

class UploadTest extends TestCase
{
    public function test_can_upload($file, $folder)
    {
        //prepare fake file
        Storage::fake('public');
        $file = UploadedFile::fake()->image('a.jpg');

        //upload with new method
        $path = FileUpload::upload($file, "images");

        //check it uploads or not
        Storage::assertExists('public/', $path);
    }
}