<?php

namespace Tests\Feature;

use App\Models\Category;
use Tests\TestCase;
use App\Models\User;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CategoryCrudTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    public function setUp() : void
    {
        parent::setUp(); 
        
        $this->user = User::factory()->create([
            'role' => 'Admin'
        ]);

    
    }

    public function test_restirct_users_to_categories_dashboard()
    {
        $this->actingAs(User::factory()->create([
            'role' => 'User'
        ]));

        $response = $this->get('/admin/categories');

        $response->assertStatus(403);
    }

    public function test_can_load_category_dashboard()
    {
        $this->actingAs($this->user);

        $response = $this->get('/admin/categories');

        $response->assertStatus(200);
    }

    public function test_can_create_new_posts()
    {
        // $Category= Category::factory()->create();

        $this->actingAs($this->user);

        $data = [
            'name' => 'cat 1'
        ];

        $response = $this->post('/admin/categories', $data);

        $response->assertStatus(302);

        $this->assertDatabaseCount('categories', 1);
        
        $category = Category::first();

        $this->assertEquals($data['name'], $category->name);
    }

    public function test_can_load_show_page()
    {
        $this->actingAs($this->user);

        $category = Category::factory()->create();

        $response = $this->get('/admin/categories/1');

        $response->assertStatus(200);

        $response->assertSeeText($category->id);
        $response->assertSeeText($category->name);
    }

    public function test_can_update_categories()
    {
        $this->actingAs($this->user);
        Category::create([
            'name' => 'Cat 1'
        ]);

        $data = [
            'name' => 'Category 1'
        ];

        $response = $this->put('/admin/categories/1', $data);

        $response->assertStatus(302);

        $category = Category::first();

        $this->assertEquals($data['name'], $category->name);
        
        $category = Category::first();

        $this->assertEquals($data['name'], $category->name);
    }

    public function test_can_delete_categories()
    {
        $this->actingAs($this->user);
        Category::create([
            'name' => 'Cat 1'
        ]);

        $response = $this->delete('/admin/categories/1');

        $response->assertStatus(302);

        $this->assertDatabaseCount('categories', 0);

    }


}