<?php

namespace Tests\Feature;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PostCrudTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create([
            'role' => 'Admin'
        ]);

        Category::factory()->create();
        
    }

    public function test_restirct_users_to_dashboard()
    {
        $this->actingAs(User::factory()->create([
            'role' => 'User'
        ]));

        $response = $this->get('/admin/posts');

        $response->assertStatus(403);
    }

    public function test_admin_can_load_post_dashboard()
    {
        $this->actingAs(User::factory()->create([
            'role' => 'Admin'
        ]));
        Category::factory()->create();
        $post = Post::factory()->create();
        $response = $this->get('/admin/posts');

        $response->assertStatus(200);

        $response->assertSeeText($post->title);
    }
    
    public function test_can_create_post_from_dashboard()
    {
        $this->actingAs($this->user);
        Category::factory()->create();

        $data = [
            'title' => 'title one',
            'body' => 'body',
            'user_id' => 1,
            'category_id' => 1,
            'is_published' => true, 
        ];

        $response = $this->post('/admin/posts', $data);
        $response->assertStatus(302);

        $this->assertDatabaseCount('posts', 1);

        $post = Post::first();

        $this->assertEquals($data['title'], $post->title);
    }


    public function test_can_load_show_page()
    {
        $this->actingAs($this->user);

        $post = Post::factory()->create();

        $response = $this->get('admin/posts/1');

        $response->assertStatus(200);
        $response->assertSeeText($post->title);
        $response->assertSeeText($post->body);
        $response->assertSeeText($post->user_id);
        $response->assertSeeText($post->category_id);
    }

    public function test_can_update_post_from_dashboard()
    {
        $this->actingAs($this->user);
        $post = Post::factory()->create();

        $data = [
            'title' => 'title 1',
            'body' => 'body',
            'category_id' => 1,
            'is_published' => true,
        ];

        $response = $this->put('/admin/posts/1', $data);

        $response->assertStatus(302);

        $post = Post::first();

        $this->assertEquals($data['title'], $post->title);
        $this->assertEquals($data['body'], $post->body);
        $this->assertEquals($data['category_id'], $post->category_id);
        $this->assertEquals($data['is_published'], $post->is_published);
    }

    public function test_can_delete_posts()
    {
        $this->actingAs($this->user);
        Post::factory()->create();

        $response = $this->delete('/admin/posts/1');

        $response->assertStatus(302);

        $this->assertDatabaseCount('posts', 0);
    }
}