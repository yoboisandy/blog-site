<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\Post;
use App\Models\User;
use App\Models\Category;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class HomepageTest extends TestCase
{
    // public function test_example()
    // {
    //     $response = $this->get('/');

    //     $response->assertStatus(200);
    // }
    
    use RefreshDatabase;

    public function test_it_loads_homepage()
    {
        $user = User::factory()->create();
        $category = Category::factory()->create();
        $post = Post::factory()->create();

        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSeeText($category->name);
        $response->assertSeeText($post->title);
        $response->assertSeeText("Continue Reading");
    }

    public function test_it_loads_page_2()
    {
        User::factory()->create();
        Category::factory()->create();
        $post_one = Post::factory()->create();
        $post_two = Post::factory()->create();
        $post_three = Post::factory()->create();
        $post_four = Post::factory()->create();
        $post_five = Post::factory()->create();

        $response_one = $this->get('/');
        $response_one->assertStatus(200);
        $response_one->assertSeeText($post_one->title);
        $response_one->assertSeeText($post_two->title);
        $response_one->assertSeeText($post_three->title);
        $response_one->assertSeeText($post_four->title);

        $response_two = $this->get('/?page=2');
        $response_two->assertStatus(200);
        $response_two->assertSeeText($post_five->title);
    }

    public function test_can_search()
    {
        $response = $this->get('/search?q=123');

        $response->assertStatus(200);
    }

    public function test_can_load_categories()
    {
        $user = User::factory()->create();
        $category = Category::factory()->create();
        $post = Post::factory()->create([

            'user_id' => $user->id,
            'category_id'=> $category->id
        ]);

        $response = $this->get('/categories/1');

        $response->assertStatus(200);

        $response->assertSeeText($category->name);

        $response->assertSeeText($post->title);
    }

    public function test_can_search_for_post()
    {
        $user = User::factory()->create();
        $category = Category::factory()->create();
        $post = Post::factory()->create([
            'title' => 'Post abc',
            'user_id' => $user->id,
            'category_id'=> $category->id
        ]);
        $response = $this->get('/search?q=abc');

        $response->assertStatus(200);

        $response->assertSeeText('Post abc');
    }
}