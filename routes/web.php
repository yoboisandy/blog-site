<?php

use App\Http\Controllers\NotificationController;
use App\Http\Controllers\SiteController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/admin', [\App\Http\Controllers\Admin\AdminController::class, 'index'])
->name('admin.index')
->middleware('auth', 'is_admin');

Route::middleware('auth', 'is_admin')->name('admin.')->prefix('admin')->group(function(){
    Route::get('/categories/search', [\App\Http\Controllers\Admin\CategoryController::class, 'search'])->name('categories.search');
    Route::resource('categories', \App\Http\Controllers\Admin\CategoryController::class);
    Route::get('/posts/search', [\App\Http\Controllers\Admin\PostController::class, 'search'])->name('posts.search');
    Route::resource('posts', \App\Http\Controllers\Admin\PostController::class);
    Route::get('/users/search', [\App\Http\Controllers\Admin\UserController::class, 'search'])->name('users.search');
    Route::resource('users', \App\Http\Controllers\Admin\UserController::class);
}); 

Route::get('/notifications', [\App\Http\Controllers\NotificationController::class, 'index']);

Route::get('/', [\App\Http\Controllers\SiteController::class, 'index'])->name('index');

Route::get('/categories/{category}', [\App\Http\Controllers\SiteController::class, 'category'])->name('category');

Route::get('/posts/{post}', [\App\Http\Controllers\SiteController::class, 'post'])->name('post');

Route::get('/search', [\App\Http\Controllers\SiteController::class, 'search'])->name('search');