<?php

namespace App\Http\Controllers\API;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use function PHPSTORM_META\map;

class DataController extends Controller
{
    public function home()
    {
        $posts = Post::select(['id', 'title', 'user_id', 'category_id'])
            ->with(['category'])
                ->get();
        $new_posts = $posts->map(function($post){
            return [
                'id' => $post->id,
                'title' => $post->title,
                'category' => $post->category->name,
            ];
        });

        return response()->json($new_posts);
    }

    public function checkPost(Request $request)
    {
        // $request->validate([
        //     'name' => ['required'],
        //     'age' => ['required']
        // ]);
        
        return response()->json($request->all());
    }
}