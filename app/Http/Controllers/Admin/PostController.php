<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\FileUpload;
use Illuminate\Support\Facades\Storage;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with(['user', 'category'])->get();
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.posts.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'title'         => ['required'],
            'body'          => ['required'],
            // 'user_id'       => ['required', 'exists:users,id'],
            'category_id'   => ['required', 'exists:categories,id'],
            'is_published'  => ['nullable', 'boolean'],
            'image'         => ['nullable', 'mimes:png,jpeg,jpg,gif'],
        ]);

        $data['user_id'] = auth()->user()->id;
        $data['is_published'] = $request->is_published ? true : false;

        if($request->hasFile('image')){
            $data['image'] = FileUpload::upload($request->file('image'), "images");
        }

        Post::create($data);
        // Post::create([
        //     'title' => $request->title,
        //     'body' => $request->body,
        //     'user_id' => auth()->user()->id,
        //     'category_id' => $request->category_id
        // ]);

        return redirect()->route('admin.posts.index')->with('success', 'Post added successully');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function edit(Post $post)
    {
        $categories = Category::all();
        return view('admin.posts.edit', compact('post', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $data = $request->validate([
            'title'         => ['required'],
            'body'          => ['required'],
            // 'user_id'       => ['required', 'exists:users,id'],
            'category_id'   => ['required', 'exists:categories,id'],
            'is_published' => ['nullable', 'boolean'],
            'image' => ['nullable', 'mimes:jpg,png,jpeg,gif'],
        ]);

        $data['is_published'] = $request->is_published ? true : false;

        if($request->hasFile('image')){
            // $ext = $request->file('image')->extension();
            // $name = Str::random(20) .".". $ext;
            // $request->file('image')->storeAs('public/images' , $name);

            // $data['image'] = "images/". $name;

            $data['image'] = FileUpload::upload($request->file('image'), "images");;

            if($post->image){
                Storage::delete('public/' . $post->image);
            }
        }

        $post->update($data);
        
        // $post->update([
        //     'title' => $request->title,
        //     'body' => $request->body,
        //     // 'user_id' => auth()->user()->id,
        //     'category_id' => $request->category_id
        // ]);

        return redirect()->route('admin.posts.index')->with('success', 'Post updated successully');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post)
    {
        $post->delete();
        return redirect()->route('admin.posts.index')->with('success', 'Post deleted successully');
    }

    public function search(Request $request)
    {
        $search = $request->q;

        $posts = Post::where('title', 'LIKE', '%'.$search.'%')->with(['user', 'category'])->get();

        return view('admin.posts.index', compact('posts'));
    }
}