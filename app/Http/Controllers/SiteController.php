<?php

namespace App\Http\Controllers;

use App\Models\Post;
use App\Models\Category;
use Illuminate\Http\Request;

class SiteController extends Controller
{
    public function index()
    {
        $categories = Category::withCount('posts')->get();
        $posts = Post::simplePaginate(4);
        return view('front.index', compact('categories', 'posts'));
    }

    public function category(Category $category)
    {
        $posts = $category->posts()->paginate(4);
        $categories = Category::withCount('posts')->get();
        return view('front.category', compact('posts','category','categories'));
    }

    public function post(Post $post)
    {
        return view('front.post', compact('post'));
    }

    public function search(Request $request)
    {
        $search = $request->q;

        $posts = Post::where('title', 'LIKE', '%'.$search.'%')->simplePaginate(4)->withQueryString();

        $categories = Category::withCount('posts')->get();
        
        return view('front.index', compact('posts', 'categories'));
    }
}