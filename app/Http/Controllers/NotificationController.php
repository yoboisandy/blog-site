<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {
        $notifications = User::find(auth()->user()->id);
        return view('notifications', compact('notifications'));
    }
}