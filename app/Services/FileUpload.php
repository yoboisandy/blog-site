<?php 

namespace App\Services;

use Illuminate\Support\Str;

class FileUpload{
    public static function upload($file, $folder)
    {
            $ext = $file->extension();
            $name = Str::random(20) .".". $ext;
            $file->storeAs('public/images' , $name);

            return $folder . "/" . $name;
    }
}